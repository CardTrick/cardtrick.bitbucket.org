var CTEdit = (function() {

    var module = {}

    var postBox = document.getElementsByName("Post")[0];
    var posting = (postBox !== undefined);
    
    
    var getColours = function() {
        var colours = {};
        var posts = document.getElementsByClassName("postcolor");
        for (var i=0; i<posts.length; i++) {
            var post = posts[i];
            var spans = post.getElementsByTagName("span");
            for (var s=0; s<spans.length; s++) {
                var span = spans[s];
                if (span.style.color != "") {
                    colours[span.style.color] = true;
                }
            }
        }
        var colourList = [];
        for (var c in colours) {
            var col = (CTTools.isRGB(c)) ? CTTools.rgbToHex(c) : c;
            colourList.push(col);
        }
        return colourList;
    };
    
    var WordCounter = function(target) {
        
    };
    
    var ColourPalette = function() {
        
    };
    
    module["getColours"] = getColours;


    return module;

})();
