var CTCollapse = (function() {

    var initialized = false;
    var boxes = [];
    
    
    var boxClosed = function(number) {
        var box = boxes[number];
        return box.style.maxHeight == "0px";
    };
    
    var boxOpen = function(number) {
        return !boxClosed(number);
    };
    
    var saveBoxStatus = function() {
        boxStatus = 0;
        for (var i=0; i<boxes.length; i++) {
            if (boxClosed(i)) {
                boxStatus |= (1<<i);
            }
        }
        CTCookie.setValue("ctCollapse", ""+boxStatus);
    };
    
    var openBox = function(number) {
        if (!initialized) {
            console.log("CTCollapse isn't initialized.");
            return;
        }
        var box = boxes[number];
        var newHeight
        box.style.maxHeight = box.scrollHeight+"px";
        box.classList.remove("ctClosed");
        saveBoxStatus();
    };
    
    var closeBox = function(number) {
        if (!initialized) {
            console.log("CTCollapse isn't initialized.");
            return;
        }
        var box = boxes[number];
        box.style.maxHeight = "0px";
        box.classList.add("ctClosed");
        saveBoxStatus();
    };
    
    var toggleBox = function(number) {
        if (boxClosed(number)) {
            openBox(number);
        } else {
            closeBox(number);
        }
    };
    
    var initCollapse = function() {
        var colBoxes = document.getElementsByClassName("ctCollapse");
        boxes = colBoxes;
        initialized = true;
        var boxStatus = parseInt(CTCookie.getValue("ctCollapse"));
        for (var i=0; i<boxes.length; i++) {
            if ((boxStatus & (1<<i)) > 0) {
                closeBox(i);
            }
        }
    };

    return {
        "initCollapse": initCollapse,
        "boxClosed": boxClosed,
        "boxOpen": boxOpen,
        "openBox": openBox,
        "closeBox": closeBox,
        "toggleBox": toggleBox
    };

})();
