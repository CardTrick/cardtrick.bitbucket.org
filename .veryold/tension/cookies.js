var CTCookie = (function() {

    var cookieLoaded = false;
    var cookieObj = null;
    var cookiePattern = /\b(\S*?)\=(\S*?)(\;|$)/g;

    var loadCookie = function() {
        cookiePattern.lastIndex = 0;
        var results = {};
        var matched = cookiePattern.exec(document.cookie);
        while (matched != null) {
            results[unescape(matched[1])] = unescape(matched[2]);
            matched = cookiePattern.exec(document.cookie);
        }
        return results;
    };
    
    var getCookie = function() {
        if (!cookieLoaded) {
            cookieObj = loadCookie();
            cookieLoaded = true;
        }
        return cookieObj;
    };
    
    var setValue = function(entry, newValue) {
        var endDate = new Date();
        endDate.setDate(endDate.getDate() + 60);
        var cookie = getCookie();
        cookie[entry] = newValue;
        document.cookie = escape(entry) + "=" + escape(newValue)
                          + ";expires="+endDate.toUTCString();
    };
    
    var getValue = function(entry, defaultValue) {
        var cookie = getCookie();
        if (cookie[entry] === undefined) {
            if (defaultValue !== undefined) {
                setValue(entry, defaultValue);
            }
            return defaultValue;
        } else {
            return cookie[entry];
        }
    };
    
    var delValue = function(entry) {
        var cookie = getCookie();
        delete cookie[entry];
        document.cookie = entry + "=; expires=" + new Date(0).toUTCString();
    };
    
    return {
        "getCookie": getCookie,
        "setValue": setValue,
        "getValue": getValue,
        "delValue": delValue
    };

})();
