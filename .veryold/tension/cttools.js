var CTTools = (function() {

    var rgbPattern = /rgb\((\d+), ?(\d+), ?(\d+)\)/i;
    
    function UpdatingClock(elementId, timezoneName) {
        this.element = document.getElementById(elementId);
        this.timezone = timezoneName;
        this.dateobj = null;
        
        this.update = function() {
            this.dateobj = Date.now();
            this.dateobj.setTimezone(this.timezone);
            this.element.innerHTML = this.getTextTime();
        };
        
        this.getTextTime = function() {
            return this.dateobj.toString("HH:mm:ss");
        };
        
        this.update();
    }

    var module = {
        rgbToHex: function(rgbColour) {
            rgbPattern.lastIndex = 0;
            var rgb = rgbPattern.exec(rgbColour).slice(1,4)
            var hexColour = "#";
            for (var x=0; x<rgb.length; x++) {
                var h = parseInt(rgb[x]).toString(16);
                hexColour += (h.length == 1) ? "0"+h : h;
            }
            return hexColour;
        },
        
        isRGB: function(colour) {
            return rgbPattern.test(colour);
        },
        
        UpdatingClock: UpdatingClock
    }
    
    return module;

})();
