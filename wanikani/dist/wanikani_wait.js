(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["wanikani_wait"] = factory();
	else
		root["wanikani_wait"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.WK = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _urlParse = __webpack_require__(1);

	var _urlParse2 = _interopRequireDefault(_urlParse);

	var _jsonp = __webpack_require__(5);

	var _jsonp2 = _interopRequireDefault(_jsonp);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var WK = exports.WK = function () {
	    function WK(devmode, offline) {
	        _classCallCheck(this, WK);

	        this.devmode = devmode;
	        this.offline = offline;
	        this.baseUrl = devmode ? "http://127.0.0.1:8000/" : "https://cardtrick.bitbucket.org/";
	        this.updateURL("");
	        this.reviews = [];
	        this.reviewDate = null;
	    }

	    _createClass(WK, [{
	        key: "apiKeyFieldChanged",
	        value: function apiKeyFieldChanged() {
	            var input = document.getElementById("api-key-field");
	            var apiKey = input.value;
	            this.updateURL(apiKey);
	        }
	    }, {
	        key: "tryCheckReviewTime",
	        value: function tryCheckReviewTime() {
	            var url = new _urlParse2.default(window.location.href, true);
	            var apiKey = url.query["api_key"];
	            if (apiKey !== undefined) {
	                this.updateURL(apiKey);
	                this.updateReviewData(apiKey);
	            }
	        }
	    }, {
	        key: "updateURL",
	        value: function updateURL(apiKey) {
	            var apiKeyLink = document.getElementById("api-key-link");
	            var url = this.baseUrl + "wanikani/?api_key=" + apiKey;
	            apiKeyLink.textContent = url;
	            apiKeyLink.href = url;
	        }
	    }, {
	        key: "updateReviewData",
	        value: function updateReviewData(apiKey) {
	            var _this = this;

	            if (this.offline) {
	                this.updateWithDevData();
	            } else {
	                (0, _jsonp2.default)("https://www.wanikani.com/api/user/" + apiKey + "/radicals/", {}, function (err, data) {
	                    return _this.receiveCharacters(err, data);
	                });
	                (0, _jsonp2.default)("https://www.wanikani.com/api/user/" + apiKey + "/kanji/", {}, function (err, data) {
	                    return _this.receiveCharacters(err, data);
	                });
	            }
	        }
	    }, {
	        key: "updateWithDevData",
	        value: function updateWithDevData() {
	            var data = { "user_information": { "username": "ChillPenguin", "gravatar": "e900aa0e98f427dc5839d08e490a9c2e", "level": 2, "title": "Guppies", "about": "", "website": null, "twitter": null, "topics_count": 0, "posts_count": 0, "creation_date": 1486431553, "vacation_date": null }, "requested_information": [{ "character": "七", "meaning": "seven", "onyomi": "しち", "kunyomi": "なな.*", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592500, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "二", "meaning": "two", "onyomi": "に", "kunyomi": "ふた.*", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592401, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "三", "meaning": "three", "onyomi": "さん", "kunyomi": "み.*", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592401, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "山", "meaning": "mountain", "onyomi": "さん", "kunyomi": "やま", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592343, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487476800, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 6, "meaning_incorrect": 0, "meaning_max_streak": 6, "meaning_current_streak": 6, "reading_correct": 6, "reading_incorrect": 1, "reading_max_streak": 3, "reading_current_streak": 3 } }, { "character": "女", "meaning": "woman", "onyomi": "じょ", "kunyomi": "おんな, め", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592496, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 5, "meaning_incorrect": 0, "meaning_max_streak": 5, "meaning_current_streak": 5, "reading_correct": 5, "reading_incorrect": 1, "reading_max_streak": 5, "reading_current_streak": 5 } }, { "character": "正", "meaning": "correct", "onyomi": "せい, しょう", "kunyomi": "ただ.しい, まさ.に", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034154, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "立", "meaning": "stand", "onyomi": "りつ", "kunyomi": "た.つ", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034163, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "玉", "meaning": "ball", "onyomi": "ぎょく", "kunyomi": "たま", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487036331, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "四", "meaning": "four", "onyomi": "し", "kunyomi": "よん, よ", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034125, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "手", "meaning": "hand", "onyomi": "しゅ", "kunyomi": "て", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034249, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "大", "meaning": "big, large", "onyomi": "たい, だい", "kunyomi": "おお", "important_reading": "onyomi", "level": 1, "nanori": "ひろ", "user_specific": { "unlocked_date": 1486592484, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487356200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "小", "meaning": "small, little", "onyomi": "しょう", "kunyomi": "ちい, こ, お", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034276, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "apprentice", "srs_numeric": 3, "available_date": 1487223000, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 1, "reading_max_streak": 3, "reading_current_streak": 1 } }, { "character": "目", "meaning": "eye", "onyomi": "もく", "kunyomi": "め", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034170, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "一", "meaning": "one", "onyomi": "いち", "kunyomi": "ひと.*", "important_reading": "onyomi", "level": 1, "nanori": "かず", "user_specific": { "unlocked_date": 1486592356, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "又", "meaning": "again", "onyomi": null, "kunyomi": "また", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034232, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "夕", "meaning": "evening", "onyomi": "せき", "kunyomi": "ゆう", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487068379, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "apprentice", "srs_numeric": 4, "available_date": 1487244600, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 3, "meaning_incorrect": 0, "meaning_max_streak": 3, "meaning_current_streak": 3, "reading_correct": 3, "reading_incorrect": 0, "reading_max_streak": 3, "reading_current_streak": 3 } }, { "character": "八", "meaning": "eight", "onyomi": "はち", "kunyomi": "や.*", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592397, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487356200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "月", "meaning": "moon, month", "onyomi": "げつ, がつ", "kunyomi": "つき", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487103113, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "apprentice", "srs_numeric": 4, "available_date": 1487273400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 3, "meaning_incorrect": 0, "meaning_max_streak": 3, "meaning_current_streak": 3, "reading_correct": 3, "reading_incorrect": 0, "reading_max_streak": 3, "reading_current_streak": 3 } }, { "character": "了", "meaning": "finish, complete, end", "onyomi": "りょう", "kunyomi": null, "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487635200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "出", "meaning": "exit", "onyomi": "しゅつ", "kunyomi": "で.る, だ.す", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487637000, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "六", "meaning": "six", "onyomi": "ろく", "kunyomi": "む.つ", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487635200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "王", "meaning": "king", "onyomi": "おう", "kunyomi": null, "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487036331, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "天", "meaning": "heaven", "onyomi": "てん", "kunyomi": "あま", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034196, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "工", "meaning": "construction, industry", "onyomi": "こう, く", "kunyomi": null, "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592407, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "川", "meaning": "river", "onyomi": "せん", "kunyomi": "かわ", "important_reading": "kunyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592358, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "力", "meaning": "power, strength", "onyomi": "りょく, りき", "kunyomi": "ちから", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592479, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "丁", "meaning": "street, ward", "onyomi": "ちょう, てい", "kunyomi": null, "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034128, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "文", "meaning": "writing, sentence", "onyomi": "ぶん, もん", "kunyomi": null, "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487068496, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "apprentice", "srs_numeric": 4, "available_date": 1487244600, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 1, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "円", "meaning": "yen, round, circle", "onyomi": "えん", "kunyomi": "まる.い", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034165, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "上", "meaning": "above, up, over", "onyomi": "じょう", "kunyomi": "うえ, あ, のぼ, うわ, かみ", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592453, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 6, "meaning_incorrect": 0, "meaning_max_streak": 6, "meaning_current_streak": 6, "reading_correct": 6, "reading_incorrect": 1, "reading_max_streak": 5, "reading_current_streak": 5 } }, { "character": "口", "meaning": "mouth", "onyomi": "こう, く", "kunyomi": "くち", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592449, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "刀", "meaning": "sword, katana", "onyomi": "とう", "kunyomi": "かたな", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034156, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "apprentice", "srs_numeric": 3, "available_date": 1487223000, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 1, "meaning_max_streak": 3, "meaning_current_streak": 1, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "五", "meaning": "five", "onyomi": "ご", "kunyomi": "いつ.つ", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034274, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "白", "meaning": "white", "onyomi": "はく", "kunyomi": "しろ, しら", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487036344, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "下", "meaning": "below, down, under, beneath", "onyomi": "か, げ", "kunyomi": "した, さが, くだ, お", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592453, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487475000, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 6, "meaning_incorrect": 1, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 6, "reading_incorrect": 0, "reading_max_streak": 6, "reading_current_streak": 6 } }, { "character": "田", "meaning": "rice paddy, rice field, field", "onyomi": "でん", "kunyomi": "た", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487036340, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "十", "meaning": "ten", "onyomi": "じゅう", "kunyomi": "とお.*", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592476, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487539800, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 6, "meaning_incorrect": 1, "meaning_max_streak": 3, "meaning_current_streak": 3, "reading_correct": 6, "reading_incorrect": 0, "reading_max_streak": 6, "reading_current_streak": 6 } }, { "character": "木", "meaning": "tree, wood", "onyomi": "もく", "kunyomi": "き, こ", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487637000, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "火", "meaning": "fire", "onyomi": "か", "kunyomi": "ひ, ほ", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034319, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "apprentice", "srs_numeric": 4, "available_date": 1487273400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 5, "meaning_incorrect": 1, "meaning_max_streak": 3, "meaning_current_streak": 3, "reading_correct": 5, "reading_incorrect": 0, "reading_max_streak": 5, "reading_current_streak": 5 } }, { "character": "子", "meaning": "child, kid", "onyomi": "し, す", "kunyomi": "こ", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034185, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "中", "meaning": "middle, in, inside, center", "onyomi": "ちゅう", "kunyomi": "なか", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034307, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "本", "meaning": "book, origin, real, main", "onyomi": "ほん", "kunyomi": "もと", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034314, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "々", "meaning": "repeater, repetition, reduplication, iteration mark", "onyomi": "のま", "kunyomi": "のま", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487669400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 5, "meaning_incorrect": 0, "meaning_max_streak": 5, "meaning_current_streak": 5, "reading_correct": 5, "reading_incorrect": 1, "reading_max_streak": 5, "reading_current_streak": 5 } }, { "character": "日", "meaning": "sun, day", "onyomi": "にち, じつ", "kunyomi": "ひ, か, び", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487637000, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "人", "meaning": "person", "onyomi": "にん, じん", "kunyomi": "ひと, と", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592389, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "入", "meaning": "enter", "onyomi": "にゅう", "kunyomi": "はい.る, い.れる", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486592466, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487428200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 7, "meaning_incorrect": 1, "meaning_max_streak": 5, "meaning_current_streak": 5, "reading_correct": 7, "reading_incorrect": 1, "reading_max_streak": 7, "reading_current_streak": 7 } }, { "character": "千", "meaning": "thousand", "onyomi": "せん", "kunyomi": "ち", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034240, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "九", "meaning": "nine", "onyomi": "く, きゅう", "kunyomi": "ここの.*", "important_reading": "onyomi", "level": 1, "nanori": null, "user_specific": { "unlocked_date": 1486717337, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487480400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "丸", "meaning": "circle, circular, round", "onyomi": "がん", "kunyomi": "まる", "important_reading": "kunyomi", "level": 2, "nanori": "", "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487635200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "右", "meaning": "right", "onyomi": "う, ゆう", "kunyomi": "みぎ", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034209, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "石", "meaning": "stone", "onyomi": "せき", "kunyomi": "いし", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034172, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "左", "meaning": "left", "onyomi": "さ", "kunyomi": "ひだり", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034209, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "土", "meaning": "soil, earth, ground, dirt", "onyomi": "ど, と", "kunyomi": "つち", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034234, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "犬", "meaning": "dog", "onyomi": "けん", "kunyomi": "いぬ", "important_reading": "kunyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487034167, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "才", "meaning": "genius", "onyomi": "さい", "kunyomi": null, "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1486879245, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487635200, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }, { "character": "水", "meaning": "water", "onyomi": "すい", "kunyomi": "みず", "important_reading": "onyomi", "level": 2, "nanori": null, "user_specific": { "unlocked_date": 1487036337, "user_synonyms": null, "meaning_note": null, "reading_note": null, "srs": "guru", "srs_numeric": 5, "available_date": 1487795400, "burned": false, "burned_date": 0, "reactivated_date": 0, "meaning_correct": 4, "meaning_incorrect": 0, "meaning_max_streak": 4, "meaning_current_streak": 4, "reading_correct": 4, "reading_incorrect": 0, "reading_max_streak": 4, "reading_current_streak": 4 } }] };
	            this.receiveCharacters(null, data);
	        }
	    }, {
	        key: "receiveCharacters",
	        value: function receiveCharacters(err, data) {
	            var _this2 = this;

	            var radicals = data["requested_information"];
	            radicals.forEach(function (r) {
	                if (r["user_specific"]["srs_numeric"] <= 4) {
	                    var newDate = r["user_specific"]["available_date"];
	                    var character = r["character"];
	                    if (_this2.reviewDate === null || newDate < _this2.reviewDate) {
	                        _this2.reviews = [character];
	                        _this2.reviewDate = newDate;
	                    } else if (_this2.reviewDate === newDate) {
	                        _this2.reviews.push(character);
	                    }
	                }
	            });
	            this.updateInfo();
	        }
	    }, {
	        key: "updateInfo",
	        value: function updateInfo() {
	            var infobox = document.getElementById("wanikani-info");

	            if (this.reviewDate === null) {
	                infobox.textContent = "No important reviews apparently!";
	                return;
	            }

	            var date = new Date(this.reviewDate * 1000);
	            var text = "<p>Your next important review is on " + date.toString() + "</p><ul>";
	            for (var i = 0; i < this.reviews.length; ++i) {
	                text += "<li>" + this.reviews[i] + "</li>";
	            }
	            text += "</ul>";

	            infobox.innerHTML = text;
	        }
	    }]);

	    return WK;
	}();

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var required = __webpack_require__(2)
	  , lolcation = __webpack_require__(3)
	  , qs = __webpack_require__(4)
	  , protocolre = /^([a-z][a-z0-9.+-]*:)?(\/\/)?([\S\s]*)/i;

	/**
	 * These are the parse rules for the URL parser, it informs the parser
	 * about:
	 *
	 * 0. The char it Needs to parse, if it's a string it should be done using
	 *    indexOf, RegExp using exec and NaN means set as current value.
	 * 1. The property we should set when parsing this value.
	 * 2. Indication if it's backwards or forward parsing, when set as number it's
	 *    the value of extra chars that should be split off.
	 * 3. Inherit from location if non existing in the parser.
	 * 4. `toLowerCase` the resulting value.
	 */
	var rules = [
	  ['#', 'hash'],                        // Extract from the back.
	  ['?', 'query'],                       // Extract from the back.
	  ['/', 'pathname'],                    // Extract from the back.
	  ['@', 'auth', 1],                     // Extract from the front.
	  [NaN, 'host', undefined, 1, 1],       // Set left over value.
	  [/:(\d+)$/, 'port', undefined, 1],    // RegExp the back.
	  [NaN, 'hostname', undefined, 1, 1]    // Set left over.
	];

	/**
	 * @typedef ProtocolExtract
	 * @type Object
	 * @property {String} protocol Protocol matched in the URL, in lowercase.
	 * @property {Boolean} slashes `true` if protocol is followed by "//", else `false`.
	 * @property {String} rest Rest of the URL that is not part of the protocol.
	 */

	/**
	 * Extract protocol information from a URL with/without double slash ("//").
	 *
	 * @param {String} address URL we want to extract from.
	 * @return {ProtocolExtract} Extracted information.
	 * @api private
	 */
	function extractProtocol(address) {
	  var match = protocolre.exec(address);

	  return {
	    protocol: match[1] ? match[1].toLowerCase() : '',
	    slashes: !!match[2],
	    rest: match[3]
	  };
	}

	/**
	 * Resolve a relative URL pathname against a base URL pathname.
	 *
	 * @param {String} relative Pathname of the relative URL.
	 * @param {String} base Pathname of the base URL.
	 * @return {String} Resolved pathname.
	 * @api private
	 */
	function resolve(relative, base) {
	  var path = (base || '/').split('/').slice(0, -1).concat(relative.split('/'))
	    , i = path.length
	    , last = path[i - 1]
	    , unshift = false
	    , up = 0;

	  while (i--) {
	    if (path[i] === '.') {
	      path.splice(i, 1);
	    } else if (path[i] === '..') {
	      path.splice(i, 1);
	      up++;
	    } else if (up) {
	      if (i === 0) unshift = true;
	      path.splice(i, 1);
	      up--;
	    }
	  }

	  if (unshift) path.unshift('');
	  if (last === '.' || last === '..') path.push('');

	  return path.join('/');
	}

	/**
	 * The actual URL instance. Instead of returning an object we've opted-in to
	 * create an actual constructor as it's much more memory efficient and
	 * faster and it pleases my OCD.
	 *
	 * @constructor
	 * @param {String} address URL we want to parse.
	 * @param {Object|String} location Location defaults for relative paths.
	 * @param {Boolean|Function} parser Parser for the query string.
	 * @api public
	 */
	function URL(address, location, parser) {
	  if (!(this instanceof URL)) {
	    return new URL(address, location, parser);
	  }

	  var relative, extracted, parse, instruction, index, key
	    , instructions = rules.slice()
	    , type = typeof location
	    , url = this
	    , i = 0;

	  //
	  // The following if statements allows this module two have compatibility with
	  // 2 different API:
	  //
	  // 1. Node.js's `url.parse` api which accepts a URL, boolean as arguments
	  //    where the boolean indicates that the query string should also be parsed.
	  //
	  // 2. The `URL` interface of the browser which accepts a URL, object as
	  //    arguments. The supplied object will be used as default values / fall-back
	  //    for relative paths.
	  //
	  if ('object' !== type && 'string' !== type) {
	    parser = location;
	    location = null;
	  }

	  if (parser && 'function' !== typeof parser) parser = qs.parse;

	  location = lolcation(location);

	  //
	  // Extract protocol information before running the instructions.
	  //
	  extracted = extractProtocol(address || '');
	  relative = !extracted.protocol && !extracted.slashes;
	  url.slashes = extracted.slashes || relative && location.slashes;
	  url.protocol = extracted.protocol || location.protocol || '';
	  address = extracted.rest;

	  //
	  // When the authority component is absent the URL starts with a path
	  // component.
	  //
	  if (!extracted.slashes) instructions[2] = [/(.*)/, 'pathname'];

	  for (; i < instructions.length; i++) {
	    instruction = instructions[i];
	    parse = instruction[0];
	    key = instruction[1];

	    if (parse !== parse) {
	      url[key] = address;
	    } else if ('string' === typeof parse) {
	      if (~(index = address.indexOf(parse))) {
	        if ('number' === typeof instruction[2]) {
	          url[key] = address.slice(0, index);
	          address = address.slice(index + instruction[2]);
	        } else {
	          url[key] = address.slice(index);
	          address = address.slice(0, index);
	        }
	      }
	    } else if (index = parse.exec(address)) {
	      url[key] = index[1];
	      address = address.slice(0, index.index);
	    }

	    url[key] = url[key] || (
	      relative && instruction[3] ? location[key] || '' : ''
	    );

	    //
	    // Hostname, host and protocol should be lowercased so they can be used to
	    // create a proper `origin`.
	    //
	    if (instruction[4]) url[key] = url[key].toLowerCase();
	  }

	  //
	  // Also parse the supplied query string in to an object. If we're supplied
	  // with a custom parser as function use that instead of the default build-in
	  // parser.
	  //
	  if (parser) url.query = parser(url.query);

	  //
	  // If the URL is relative, resolve the pathname against the base URL.
	  //
	  if (
	      relative
	    && location.slashes
	    && url.pathname.charAt(0) !== '/'
	    && (url.pathname !== '' || location.pathname !== '')
	  ) {
	    url.pathname = resolve(url.pathname, location.pathname);
	  }

	  //
	  // We should not add port numbers if they are already the default port number
	  // for a given protocol. As the host also contains the port number we're going
	  // override it with the hostname which contains no port number.
	  //
	  if (!required(url.port, url.protocol)) {
	    url.host = url.hostname;
	    url.port = '';
	  }

	  //
	  // Parse down the `auth` for the username and password.
	  //
	  url.username = url.password = '';
	  if (url.auth) {
	    instruction = url.auth.split(':');
	    url.username = instruction[0] || '';
	    url.password = instruction[1] || '';
	  }

	  url.origin = url.protocol && url.host && url.protocol !== 'file:'
	    ? url.protocol +'//'+ url.host
	    : 'null';

	  //
	  // The href is just the compiled result.
	  //
	  url.href = url.toString();
	}

	/**
	 * This is convenience method for changing properties in the URL instance to
	 * insure that they all propagate correctly.
	 *
	 * @param {String} part          Property we need to adjust.
	 * @param {Mixed} value          The newly assigned value.
	 * @param {Boolean|Function} fn  When setting the query, it will be the function
	 *                               used to parse the query.
	 *                               When setting the protocol, double slash will be
	 *                               removed from the final url if it is true.
	 * @returns {URL}
	 * @api public
	 */
	URL.prototype.set = function set(part, value, fn) {
	  var url = this;

	  switch (part) {
	    case 'query':
	      if ('string' === typeof value && value.length) {
	        value = (fn || qs.parse)(value);
	      }

	      url[part] = value;
	      break;

	    case 'port':
	      url[part] = value;

	      if (!required(value, url.protocol)) {
	        url.host = url.hostname;
	        url[part] = '';
	      } else if (value) {
	        url.host = url.hostname +':'+ value;
	      }

	      break;

	    case 'hostname':
	      url[part] = value;

	      if (url.port) value += ':'+ url.port;
	      url.host = value;
	      break;

	    case 'host':
	      url[part] = value;

	      if (/:\d+$/.test(value)) {
	        value = value.split(':');
	        url.port = value.pop();
	        url.hostname = value.join(':');
	      } else {
	        url.hostname = value;
	        url.port = '';
	      }

	      break;

	    case 'protocol':
	      url.protocol = value.toLowerCase();
	      url.slashes = !fn;
	      break;

	    case 'pathname':
	      url.pathname = value.length && value.charAt(0) !== '/' ? '/' + value : value;

	      break;

	    default:
	      url[part] = value;
	  }

	  for (var i = 0; i < rules.length; i++) {
	    var ins = rules[i];

	    if (ins[4]) url[ins[1]] = url[ins[1]].toLowerCase();
	  }

	  url.origin = url.protocol && url.host && url.protocol !== 'file:'
	    ? url.protocol +'//'+ url.host
	    : 'null';

	  url.href = url.toString();

	  return url;
	};

	/**
	 * Transform the properties back in to a valid and full URL string.
	 *
	 * @param {Function} stringify Optional query stringify function.
	 * @returns {String}
	 * @api public
	 */
	URL.prototype.toString = function toString(stringify) {
	  if (!stringify || 'function' !== typeof stringify) stringify = qs.stringify;

	  var query
	    , url = this
	    , protocol = url.protocol;

	  if (protocol && protocol.charAt(protocol.length - 1) !== ':') protocol += ':';

	  var result = protocol + (url.slashes ? '//' : '');

	  if (url.username) {
	    result += url.username;
	    if (url.password) result += ':'+ url.password;
	    result += '@';
	  }

	  result += url.host + url.pathname;

	  query = 'object' === typeof url.query ? stringify(url.query) : url.query;
	  if (query) result += '?' !== query.charAt(0) ? '?'+ query : query;

	  if (url.hash) result += url.hash;

	  return result;
	};

	//
	// Expose the URL parser and some additional properties that might be useful for
	// others or testing.
	//
	URL.extractProtocol = extractProtocol;
	URL.location = lolcation;
	URL.qs = qs;

	module.exports = URL;


/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';

	/**
	 * Check if we're required to add a port number.
	 *
	 * @see https://url.spec.whatwg.org/#default-port
	 * @param {Number|String} port Port number we need to check
	 * @param {String} protocol Protocol we need to check against.
	 * @returns {Boolean} Is it a default port for the given protocol
	 * @api private
	 */
	module.exports = function required(port, protocol) {
	  protocol = protocol.split(':')[0];
	  port = +port;

	  if (!port) return false;

	  switch (protocol) {
	    case 'http':
	    case 'ws':
	    return port !== 80;

	    case 'https':
	    case 'wss':
	    return port !== 443;

	    case 'ftp':
	    return port !== 21;

	    case 'gopher':
	    return port !== 70;

	    case 'file':
	    return false;
	  }

	  return port !== 0;
	};


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';

	var slashes = /^[A-Za-z][A-Za-z0-9+-.]*:\/\//;

	/**
	 * These properties should not be copied or inherited from. This is only needed
	 * for all non blob URL's as a blob URL does not include a hash, only the
	 * origin.
	 *
	 * @type {Object}
	 * @private
	 */
	var ignore = { hash: 1, query: 1 }
	  , URL;

	/**
	 * The location object differs when your code is loaded through a normal page,
	 * Worker or through a worker using a blob. And with the blobble begins the
	 * trouble as the location object will contain the URL of the blob, not the
	 * location of the page where our code is loaded in. The actual origin is
	 * encoded in the `pathname` so we can thankfully generate a good "default"
	 * location from it so we can generate proper relative URL's again.
	 *
	 * @param {Object|String} loc Optional default location object.
	 * @returns {Object} lolcation object.
	 * @api public
	 */
	module.exports = function lolcation(loc) {
	  loc = loc || global.location || {};
	  URL = URL || __webpack_require__(1);

	  var finaldestination = {}
	    , type = typeof loc
	    , key;

	  if ('blob:' === loc.protocol) {
	    finaldestination = new URL(unescape(loc.pathname), {});
	  } else if ('string' === type) {
	    finaldestination = new URL(loc, {});
	    for (key in ignore) delete finaldestination[key];
	  } else if ('object' === type) {
	    for (key in loc) {
	      if (key in ignore) continue;
	      finaldestination[key] = loc[key];
	    }

	    if (finaldestination.slashes === undefined) {
	      finaldestination.slashes = slashes.test(loc.href);
	    }
	  }

	  return finaldestination;
	};

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 4 */
/***/ function(module, exports) {

	'use strict';

	var has = Object.prototype.hasOwnProperty;

	/**
	 * Simple query string parser.
	 *
	 * @param {String} query The query string that needs to be parsed.
	 * @returns {Object}
	 * @api public
	 */
	function querystring(query) {
	  var parser = /([^=?&]+)=?([^&]*)/g
	    , result = {}
	    , part;

	  //
	  // Little nifty parsing hack, leverage the fact that RegExp.exec increments
	  // the lastIndex property so we can continue executing this loop until we've
	  // parsed all results.
	  //
	  for (;
	    part = parser.exec(query);
	    result[decodeURIComponent(part[1])] = decodeURIComponent(part[2])
	  );

	  return result;
	}

	/**
	 * Transform a query string to an object.
	 *
	 * @param {Object} obj Object that should be transformed.
	 * @param {String} prefix Optional prefix.
	 * @returns {String}
	 * @api public
	 */
	function querystringify(obj, prefix) {
	  prefix = prefix || '';

	  var pairs = [];

	  //
	  // Optionally prefix with a '?' if needed
	  //
	  if ('string' !== typeof prefix) prefix = '?';

	  for (var key in obj) {
	    if (has.call(obj, key)) {
	      pairs.push(encodeURIComponent(key) +'='+ encodeURIComponent(obj[key]));
	    }
	  }

	  return pairs.length ? prefix + pairs.join('&') : '';
	}

	//
	// Expose the module.
	//
	exports.stringify = querystringify;
	exports.parse = querystring;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Module dependencies
	 */

	var debug = __webpack_require__(6)('jsonp');

	/**
	 * Module exports.
	 */

	module.exports = jsonp;

	/**
	 * Callback index.
	 */

	var count = 0;

	/**
	 * Noop function.
	 */

	function noop(){}

	/**
	 * JSONP handler
	 *
	 * Options:
	 *  - param {String} qs parameter (`callback`)
	 *  - prefix {String} qs parameter (`__jp`)
	 *  - name {String} qs parameter (`prefix` + incr)
	 *  - timeout {Number} how long after a timeout error is emitted (`60000`)
	 *
	 * @param {String} url
	 * @param {Object|Function} optional options / callback
	 * @param {Function} optional callback
	 */

	function jsonp(url, opts, fn){
	  if ('function' == typeof opts) {
	    fn = opts;
	    opts = {};
	  }
	  if (!opts) opts = {};

	  var prefix = opts.prefix || '__jp';

	  // use the callback name that was passed if one was provided.
	  // otherwise generate a unique name by incrementing our counter.
	  var id = opts.name || (prefix + (count++));

	  var param = opts.param || 'callback';
	  var timeout = null != opts.timeout ? opts.timeout : 60000;
	  var enc = encodeURIComponent;
	  var target = document.getElementsByTagName('script')[0] || document.head;
	  var script;
	  var timer;


	  if (timeout) {
	    timer = setTimeout(function(){
	      cleanup();
	      if (fn) fn(new Error('Timeout'));
	    }, timeout);
	  }

	  function cleanup(){
	    if (script.parentNode) script.parentNode.removeChild(script);
	    window[id] = noop;
	    if (timer) clearTimeout(timer);
	  }

	  function cancel(){
	    if (window[id]) {
	      cleanup();
	    }
	  }

	  window[id] = function(data){
	    debug('jsonp got', data);
	    cleanup();
	    if (fn) fn(null, data);
	  };

	  // add qs component
	  url += (~url.indexOf('?') ? '&' : '?') + param + '=' + enc(id);
	  url = url.replace('?&', '?');

	  debug('jsonp req "%s"', url);

	  // create script
	  script = document.createElement('script');
	  script.src = url;
	  target.parentNode.insertBefore(script, target);

	  return cancel;
	}


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * This is the web browser implementation of `debug()`.
	 *
	 * Expose `debug()` as the module.
	 */

	exports = module.exports = __webpack_require__(8);
	exports.log = log;
	exports.formatArgs = formatArgs;
	exports.save = save;
	exports.load = load;
	exports.useColors = useColors;
	exports.storage = 'undefined' != typeof chrome
	               && 'undefined' != typeof chrome.storage
	                  ? chrome.storage.local
	                  : localstorage();

	/**
	 * Colors.
	 */

	exports.colors = [
	  'lightseagreen',
	  'forestgreen',
	  'goldenrod',
	  'dodgerblue',
	  'darkorchid',
	  'crimson'
	];

	/**
	 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
	 * and the Firebug extension (any Firefox version) are known
	 * to support "%c" CSS customizations.
	 *
	 * TODO: add a `localStorage` variable to explicitly enable/disable colors
	 */

	function useColors() {
	  // NB: In an Electron preload script, document will be defined but not fully
	  // initialized. Since we know we're in Chrome, we'll just detect this case
	  // explicitly
	  if (typeof window !== 'undefined' && window && typeof window.process !== 'undefined' && window.process.type === 'renderer') {
	    return true;
	  }

	  // is webkit? http://stackoverflow.com/a/16459606/376773
	  // document is undefined in react-native: https://github.com/facebook/react-native/pull/1632
	  return (typeof document !== 'undefined' && document && 'WebkitAppearance' in document.documentElement.style) ||
	    // is firebug? http://stackoverflow.com/a/398120/376773
	    (typeof window !== 'undefined' && window && window.console && (console.firebug || (console.exception && console.table))) ||
	    // is firefox >= v31?
	    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
	    (typeof navigator !== 'undefined' && navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31) ||
	    // double check webkit in userAgent just in case we are in a worker
	    (typeof navigator !== 'undefined' && navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/));
	}

	/**
	 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
	 */

	exports.formatters.j = function(v) {
	  try {
	    return JSON.stringify(v);
	  } catch (err) {
	    return '[UnexpectedJSONParseError]: ' + err.message;
	  }
	};


	/**
	 * Colorize log arguments if enabled.
	 *
	 * @api public
	 */

	function formatArgs(args) {
	  var useColors = this.useColors;

	  args[0] = (useColors ? '%c' : '')
	    + this.namespace
	    + (useColors ? ' %c' : ' ')
	    + args[0]
	    + (useColors ? '%c ' : ' ')
	    + '+' + exports.humanize(this.diff);

	  if (!useColors) return;

	  var c = 'color: ' + this.color;
	  args.splice(1, 0, c, 'color: inherit')

	  // the final "%c" is somewhat tricky, because there could be other
	  // arguments passed either before or after the %c, so we need to
	  // figure out the correct index to insert the CSS into
	  var index = 0;
	  var lastC = 0;
	  args[0].replace(/%[a-zA-Z%]/g, function(match) {
	    if ('%%' === match) return;
	    index++;
	    if ('%c' === match) {
	      // we only are interested in the *last* %c
	      // (the user may have provided their own)
	      lastC = index;
	    }
	  });

	  args.splice(lastC, 0, c);
	}

	/**
	 * Invokes `console.log()` when available.
	 * No-op when `console.log` is not a "function".
	 *
	 * @api public
	 */

	function log() {
	  // this hackery is required for IE8/9, where
	  // the `console.log` function doesn't have 'apply'
	  return 'object' === typeof console
	    && console.log
	    && Function.prototype.apply.call(console.log, console, arguments);
	}

	/**
	 * Save `namespaces`.
	 *
	 * @param {String} namespaces
	 * @api private
	 */

	function save(namespaces) {
	  try {
	    if (null == namespaces) {
	      exports.storage.removeItem('debug');
	    } else {
	      exports.storage.debug = namespaces;
	    }
	  } catch(e) {}
	}

	/**
	 * Load `namespaces`.
	 *
	 * @return {String} returns the previously persisted debug modes
	 * @api private
	 */

	function load() {
	  try {
	    return exports.storage.debug;
	  } catch(e) {}

	  // If debug isn't set in LS, and we're in Electron, try to load $DEBUG
	  if (typeof process !== 'undefined' && 'env' in process) {
	    return process.env.DEBUG;
	  }
	}

	/**
	 * Enable namespaces listed in `localStorage.debug` initially.
	 */

	exports.enable(load());

	/**
	 * Localstorage attempts to return the localstorage.
	 *
	 * This is necessary because safari throws
	 * when a user disables cookies/localstorage
	 * and you attempt to access it.
	 *
	 * @return {LocalStorage}
	 * @api private
	 */

	function localstorage() {
	  try {
	    return window.localStorage;
	  } catch (e) {}
	}

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(7)))

/***/ },
/* 7 */
/***/ function(module, exports) {

	// shim for using process in browser
	var process = module.exports = {};

	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.

	var cachedSetTimeout;
	var cachedClearTimeout;

	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout () {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	} ())
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch(e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch(e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }


	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }



	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	
	/**
	 * This is the common logic for both the Node.js and web browser
	 * implementations of `debug()`.
	 *
	 * Expose `debug()` as the module.
	 */

	exports = module.exports = createDebug.debug = createDebug['default'] = createDebug;
	exports.coerce = coerce;
	exports.disable = disable;
	exports.enable = enable;
	exports.enabled = enabled;
	exports.humanize = __webpack_require__(9);

	/**
	 * The currently active debug mode names, and names to skip.
	 */

	exports.names = [];
	exports.skips = [];

	/**
	 * Map of special "%n" handling functions, for the debug "format" argument.
	 *
	 * Valid key names are a single, lower or upper-case letter, i.e. "n" and "N".
	 */

	exports.formatters = {};

	/**
	 * Previous log timestamp.
	 */

	var prevTime;

	/**
	 * Select a color.
	 * @param {String} namespace
	 * @return {Number}
	 * @api private
	 */

	function selectColor(namespace) {
	  var hash = 0, i;

	  for (i in namespace) {
	    hash  = ((hash << 5) - hash) + namespace.charCodeAt(i);
	    hash |= 0; // Convert to 32bit integer
	  }

	  return exports.colors[Math.abs(hash) % exports.colors.length];
	}

	/**
	 * Create a debugger with the given `namespace`.
	 *
	 * @param {String} namespace
	 * @return {Function}
	 * @api public
	 */

	function createDebug(namespace) {

	  function debug() {
	    // disabled?
	    if (!debug.enabled) return;

	    var self = debug;

	    // set `diff` timestamp
	    var curr = +new Date();
	    var ms = curr - (prevTime || curr);
	    self.diff = ms;
	    self.prev = prevTime;
	    self.curr = curr;
	    prevTime = curr;

	    // turn the `arguments` into a proper Array
	    var args = new Array(arguments.length);
	    for (var i = 0; i < args.length; i++) {
	      args[i] = arguments[i];
	    }

	    args[0] = exports.coerce(args[0]);

	    if ('string' !== typeof args[0]) {
	      // anything else let's inspect with %O
	      args.unshift('%O');
	    }

	    // apply any `formatters` transformations
	    var index = 0;
	    args[0] = args[0].replace(/%([a-zA-Z%])/g, function(match, format) {
	      // if we encounter an escaped % then don't increase the array index
	      if (match === '%%') return match;
	      index++;
	      var formatter = exports.formatters[format];
	      if ('function' === typeof formatter) {
	        var val = args[index];
	        match = formatter.call(self, val);

	        // now we need to remove `args[index]` since it's inlined in the `format`
	        args.splice(index, 1);
	        index--;
	      }
	      return match;
	    });

	    // apply env-specific formatting (colors, etc.)
	    exports.formatArgs.call(self, args);

	    var logFn = debug.log || exports.log || console.log.bind(console);
	    logFn.apply(self, args);
	  }

	  debug.namespace = namespace;
	  debug.enabled = exports.enabled(namespace);
	  debug.useColors = exports.useColors();
	  debug.color = selectColor(namespace);

	  // env-specific initialization logic for debug instances
	  if ('function' === typeof exports.init) {
	    exports.init(debug);
	  }

	  return debug;
	}

	/**
	 * Enables a debug mode by namespaces. This can include modes
	 * separated by a colon and wildcards.
	 *
	 * @param {String} namespaces
	 * @api public
	 */

	function enable(namespaces) {
	  exports.save(namespaces);

	  exports.names = [];
	  exports.skips = [];

	  var split = (namespaces || '').split(/[\s,]+/);
	  var len = split.length;

	  for (var i = 0; i < len; i++) {
	    if (!split[i]) continue; // ignore empty strings
	    namespaces = split[i].replace(/\*/g, '.*?');
	    if (namespaces[0] === '-') {
	      exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
	    } else {
	      exports.names.push(new RegExp('^' + namespaces + '$'));
	    }
	  }
	}

	/**
	 * Disable debug output.
	 *
	 * @api public
	 */

	function disable() {
	  exports.enable('');
	}

	/**
	 * Returns true if the given mode name is enabled, false otherwise.
	 *
	 * @param {String} name
	 * @return {Boolean}
	 * @api public
	 */

	function enabled(name) {
	  var i, len;
	  for (i = 0, len = exports.skips.length; i < len; i++) {
	    if (exports.skips[i].test(name)) {
	      return false;
	    }
	  }
	  for (i = 0, len = exports.names.length; i < len; i++) {
	    if (exports.names[i].test(name)) {
	      return true;
	    }
	  }
	  return false;
	}

	/**
	 * Coerce `val`.
	 *
	 * @param {Mixed} val
	 * @return {Mixed}
	 * @api private
	 */

	function coerce(val) {
	  if (val instanceof Error) return val.stack || val.message;
	  return val;
	}


/***/ },
/* 9 */
/***/ function(module, exports) {

	/**
	 * Helpers.
	 */

	var s = 1000
	var m = s * 60
	var h = m * 60
	var d = h * 24
	var y = d * 365.25

	/**
	 * Parse or format the given `val`.
	 *
	 * Options:
	 *
	 *  - `long` verbose formatting [false]
	 *
	 * @param {String|Number} val
	 * @param {Object} options
	 * @throws {Error} throw an error if val is not a non-empty string or a number
	 * @return {String|Number}
	 * @api public
	 */

	module.exports = function (val, options) {
	  options = options || {}
	  var type = typeof val
	  if (type === 'string' && val.length > 0) {
	    return parse(val)
	  } else if (type === 'number' && isNaN(val) === false) {
	    return options.long ?
				fmtLong(val) :
				fmtShort(val)
	  }
	  throw new Error('val is not a non-empty string or a valid number. val=' + JSON.stringify(val))
	}

	/**
	 * Parse the given `str` and return milliseconds.
	 *
	 * @param {String} str
	 * @return {Number}
	 * @api private
	 */

	function parse(str) {
	  str = String(str)
	  if (str.length > 10000) {
	    return
	  }
	  var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(str)
	  if (!match) {
	    return
	  }
	  var n = parseFloat(match[1])
	  var type = (match[2] || 'ms').toLowerCase()
	  switch (type) {
	    case 'years':
	    case 'year':
	    case 'yrs':
	    case 'yr':
	    case 'y':
	      return n * y
	    case 'days':
	    case 'day':
	    case 'd':
	      return n * d
	    case 'hours':
	    case 'hour':
	    case 'hrs':
	    case 'hr':
	    case 'h':
	      return n * h
	    case 'minutes':
	    case 'minute':
	    case 'mins':
	    case 'min':
	    case 'm':
	      return n * m
	    case 'seconds':
	    case 'second':
	    case 'secs':
	    case 'sec':
	    case 's':
	      return n * s
	    case 'milliseconds':
	    case 'millisecond':
	    case 'msecs':
	    case 'msec':
	    case 'ms':
	      return n
	    default:
	      return undefined
	  }
	}

	/**
	 * Short format for `ms`.
	 *
	 * @param {Number} ms
	 * @return {String}
	 * @api private
	 */

	function fmtShort(ms) {
	  if (ms >= d) {
	    return Math.round(ms / d) + 'd'
	  }
	  if (ms >= h) {
	    return Math.round(ms / h) + 'h'
	  }
	  if (ms >= m) {
	    return Math.round(ms / m) + 'm'
	  }
	  if (ms >= s) {
	    return Math.round(ms / s) + 's'
	  }
	  return ms + 'ms'
	}

	/**
	 * Long format for `ms`.
	 *
	 * @param {Number} ms
	 * @return {String}
	 * @api private
	 */

	function fmtLong(ms) {
	  return plural(ms, d, 'day') ||
	    plural(ms, h, 'hour') ||
	    plural(ms, m, 'minute') ||
	    plural(ms, s, 'second') ||
	    ms + ' ms'
	}

	/**
	 * Pluralization helper.
	 */

	function plural(ms, n, name) {
	  if (ms < n) {
	    return
	  }
	  if (ms < n * 1.5) {
	    return Math.floor(ms / n) + ' ' + name
	  }
	  return Math.ceil(ms / n) + ' ' + name + 's'
	}


/***/ }
/******/ ])
});
;