var path = require('path');

module.exports = {
    entry: './src/wanikani_wait.js',
    output: {
        filename: 'wanikani_wait.js',
        path: './dist',
        library: "wanikani_wait",
        libraryTarget: "umd"
    },
    module: {
        loaders: [
            { 
                test: path.join(__dirname, 'src'),
                loader: "babel-loader",
                query: {
                    presets: 'es2015'
                }
            }
        ]
    }
}
